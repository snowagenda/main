import React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
//import StImg from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi people</h1>
    <p>Welcome to Snow Agenda.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
		 <StaticImage
					src="../images/gotsnowagenda.png"
					alt="SnowAgenda"
					placeholder="blurred"
					layout="fixed"
					width={300}
					height={221}
			/>
    </div>
  </Layout>
)

export default IndexPage
