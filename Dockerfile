FROM node:erbium
COPY ./ /workdir
WORKDIR /workdir
#RUN npm i -g yarn
RUN curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --nightly
#RUN npm i -g react
RUN yarn global add react gatsby-cli
#RUN npm i
RUN yarn
RUN ./node_modules/.bin/gatsby clean
RUN ./node_modules/.bin/gatsby info
#RUN ./node_modules/.bin/gatsby build --prefix-paths
